def fibonacci_sum():
    """Sum the even numbers in the Fibonacci sequence that does not exceed 4 million"""
    fibonacci = [1,2]
    fibonacci_sum = 2
    while fibonacci[-2]+fibonacci[-1] <= 4000000:
        fibonacci.append(fibonacci[-2]+fibonacci[-1])
        if fibonacci[-1]%2 == 0:
            fibonacci_sum += fibonacci[-1]
    return fibonacci_sum
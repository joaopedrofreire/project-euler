def multiples_3_5():
    """Sum all the multiples of 3 or 5 below 1000"""
    multiples_sum = 0
    for i in range(1000):
        if i%3 == 0 or i%5 == 0:
            multiples_sum += i
    return multiples_sum